from PyQt5.QtCore import QThread, pyqtSignal


class UpdateInsertThread(QThread):
    update_insert = pyqtSignal()

    def __init__(self):
        QThread.__init__(self)

    def run(self):
        self.update_insert.emit()

