import csv
import shutil
import time

import keras.metrics
import tensorflow
import pickle
import numpy as np
import os
import re
import tkinter as tk
import glob

from keras.optimizer_v2.adam import Adam
from keras.preprocessing.text import Tokenizer
from keras.layers import Embedding, LSTM, Dense
from keras.models import Sequential, load_model
from keras.callbacks import ModelCheckpoint
from keras.utils.np_utils import to_categorical
from tkinter import filedialog
from sklearn.model_selection import train_test_split

from log import Log
from log_type import LogType


class NextWordPrediction:
    def __init__(self):
        self.log = Log()
        self.model = None
        self.tokenizer = None

    def __configure(self):
        os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
        self.log.add_log(LogType.INFO, "Configuring system...")

        if os.environ['CUDA_VISIBLE_DEVICES'] == '-1':
            self.log.add_log(LogType.INFO, "CPU selected")
        elif os.environ['CUDA_VISIBLE_DEVICES'] == '0':
            self.log.add_log(LogType.INFO, "GPU selected")

        physical_devices = tensorflow.config.experimental.list_physical_devices('GPU')
        if physical_devices:
            tensorflow.config.experimental.set_memory_growth(physical_devices[0], False)

    def __get_data_from_directory(self):
        self.log.add_log(LogType.INFO, "Getting data from directory...")
        data = ""
        data_directory = "Data"
        file_datas = []

        for file_name in os.listdir(data_directory):
            file_path = data_directory + '\\' + file_name

            file = None

            try:
                file = open(file_path, "r", encoding="utf8")
            except Exception as e:
                self.log.add_log(LogType.ERROR, f"Error occurred while trying to open \"{file_path}\" file: {str(e)}")

            if not file:
                continue

            lines = []
            for i in file:
                lines.append(i)

            file_data = ""

            for i in lines:
                file_data = ' '.join(lines)

            file_datas.append(file_data)

        for file_data in file_datas:
            data = ' '.join(file_datas)

        if data == "":
            return False, data

        data = re.sub("[\\s\\d\\\\\"\\[\\]+.*;:|!=@#$%^&()?/~{}]", ' ', data)

        data = data.split()
        data = ' '.join(data)

        return True, data

    def __create_tokenizer(self, data):
        self.log.add_log(LogType.INFO, "Creating tokenizer...")
        tokenizer = Tokenizer()
        token_path = 'token.pkl'

        tokenizer.fit_on_texts([data])

        pickle.dump(tokenizer, open(token_path, 'wb'))

        return tokenizer

    def __process_data(self, data):
        self.log.add_log(LogType.INFO, "Processing data...")

        # create list of sequences
        sequence_data = self.tokenizer.texts_to_sequences([data])[0]

        sequences = []

        for i in range(3, len(sequence_data)):
            words = sequence_data[i - 3:i + 1]
            sequences.append(words)

        sequences = np.array(sequences)

        x = []
        y = []

        for i in sequences:
            x.append(i[0:3])
            y.append(i[3])

        x = np.array(x)
        y = np.array(y)

        return x, y

    def __split_data(self, x, y):
        self.log.add_log(LogType.INFO, "Splitting dataset to sets...")
        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.1)

        return x_train, x_test, y_train, y_test

    def __delete_model_file(self):
        self.log.add_log(LogType.INFO, "Deleting old model...")
        model_path = 'next_words.h5'
        if os.path.exists(model_path):
            os.remove(model_path)

    def __delete_tokenizer_file(self):
        self.log.add_log(LogType.INFO, "Deleting old tokenizer...")
        tokenizer_path = 'token.pkl'
        if os.path.exists(tokenizer_path):
            os.remove(tokenizer_path)

    def __delete_datasets_csv_files(self):
        self.log.add_log(LogType.INFO, "Deleting csv datasets...")
        x_train_path = 'x_train.csv'
        y_train_path = 'y_train.csv'
        x_test_path = 'x_test.csv'
        y_test_path = 'y_test.csv'

        if os.path.exists(x_train_path):
            os.remove(x_train_path)
        if os.path.exists(y_train_path):
            os.remove(y_train_path)
        if os.path.exists(x_test_path):
            os.remove(x_test_path)
        if os.path.exists(y_test_path):
            os.remove(y_test_path)

    def __save_data_to_csv(self, x, y, filename_x, filename_y):
        self.log.add_log(LogType.INFO, f"Saving sets to \"{filename_x}\" and \"{filename_y}\" files...")

        try:
            with open(filename_x, 'w', newline="") as file:
                csvwriter = csv.writer(file)
                csvwriter.writerows(x)
        except Exception as e:
            self.log.add_log(LogType.ERROR, f"Error occurred while trying to save data at \"{filename_x}\" file:"
                                            f" {str(e)}")
            return False

        try:
            with open(filename_y, 'w', newline="") as file:
                csvwriter = csv.writer(file)
                csvwriter.writerow(y)
        except Exception as e:
            self.log.add_log(LogType.ERROR, f"Error occurred while trying to save data at \"{filename_y}\" file:"
                                            f" {str(e)}")
            return False

        return True

    def __load_data_from_csv(self, filename_x, filename_y):
        self.log.add_log(LogType.INFO, f"Loading sets from \"{filename_x}\" and \"{filename_y}\" files...")

        x = []
        y = []

        try:
            file = open(filename_x)
        except Exception as e:
            self.log.add_log(LogType.ERROR, f"Error occurred while trying to open \"{filename_x}\" file: {str(e)}")
            return False, x, y

        csvreader = csv.reader(file)
        for row in csvreader:
            x.append(row)
        file.close()

        try:
            file = open(filename_y)
        except Exception as e:
            self.log.add_log(LogType.ERROR, f"Error occurred while trying to open \"{filename_x}\" file: {str(e)}")
            return False, x, y

        csvreader = csv.reader(file)
        for row in csvreader:
            y.append(row)
        file.close()

        x = np.array(x).astype(int)
        y = np.array(y)[0].astype(int)

        return True, x, y

    def __create_model(self, vocabulary_size):
        self.log.add_log(LogType.INFO, "Creating model...")
        model_filepath = "next_words.h5"
        model_created, self.model = self.__return_new_model(vocabulary_size)
        if not model_created:
            return False

        try:
            checkpoint = ModelCheckpoint(filepath=model_filepath, monitor='loss', verbose=1, save_best_only=True)
            self.model.compile(loss="categorical_crossentropy", optimizer=Adam(learning_rate=0.001),
                               metrics=[keras.metrics.categorical_accuracy, keras.metrics.TopKCategoricalAccuracy(k=10)])
        except Exception as e:
            self.log.add_log(LogType.EXCEPTION, f"Exception occurred while creating model: {str(e)}")
            return False

        return True, checkpoint

    def __return_new_model(self, vocabulary_size):
        try:
            model = Sequential()
            model.add(Embedding(vocabulary_size, 64, input_length=3))
            model.add(LSTM(64))
            model.add(Dense(vocabulary_size, activation="relu"))
            model.add(Dense(vocabulary_size, activation="softmax"))
        except Exception as e:
            self.log.add_log(LogType.EXCEPTION, f"Exception occurred while creating model: {str(e)}")
            return False

        return True, model

    def __train_model(self, x, y, vocabulary_size, checkpoint):
        y = to_categorical(y, num_classes=vocabulary_size)
        self.log.add_log(LogType.INFO, "Training model...")

        try:
            start = time.time()
            self.model.fit(x, y, epochs=100, batch_size=64, callbacks=[checkpoint])
            end = time.time()
            measured_time = round(end-start, 3)
            self.log.add_log(LogType.INFO, f"Network learning time: {measured_time}s")
        except Exception as e:
            self.log.add_log(LogType.EXCEPTION, f"Error occurred while learning model: {str(e)}")
            return False

        self.log.add_log(LogType.INFO, "Training completed!")
        self.log.add_log(LogType.INFO, f"Learned network saved at: {checkpoint.filepath}")

        return True

    def __test_model(self, x_test, y_test):
        self.log.add_log(LogType.INFO, "Testing learned model...")

        if not self.model:
            self.log.add_log(LogType.WARNING, "Model does not exist. Operation canceled.")
            return False

        vocabulary_size = len(self.tokenizer.word_index) + 1
        y_test = to_categorical(y_test, num_classes=vocabulary_size)
        y_predicted = self.model.predict(x_test)

        m = keras.metrics.CategoricalAccuracy()
        m.update_state(y_test, y_predicted)
        accuracy_score = round(m.result().numpy() * 100, 3)
        self.log.add_log(LogType.INFO, f"Accuracy testing score for exact match: {accuracy_score}%")

        m = keras.metrics.TopKCategoricalAccuracy(10)
        m.update_state(y_test, y_predicted)
        accuracy_score = round(m.result().numpy() * 100, 3)
        self.log.add_log(LogType.INFO, f"Accuracy testing score for best 10 match: {accuracy_score}%")

        return True

    def __load_model_from_file(self):
        model_path = 'next_words.h5'

        self.log.add_log(LogType.INFO, f"Loading model from: {model_path}")

        try:
            self.model = load_model(model_path)
        except Exception as e:
            self.log.add_log(LogType.ERROR, f"Error occurred while trying to load model \"{model_path}\" from file: "
                                            f"{str(e)}")
            return False

        return True

    def __load_tokenizer_from_file(self):
        tokenizer_path = 'token.pkl'

        self.log.add_log(LogType.INFO, f"Loading tokenizer from: {tokenizer_path}")

        try:
            self.tokenizer = pickle.load(open(tokenizer_path, 'rb'))
        except Exception as e:
            self.log.add_log(LogType.ERROR, f"Error occurred while trying to load tokenizer \"{tokenizer_path}\" "
                                            f"from file: {str(e)}")
            return False

        return True

    def __predict_next_words(self, text):
        sequence = self.tokenizer.texts_to_sequences([text])
        sequence = np.array(sequence)

        best_predictions = []
        predicted_words = []

        predictions = self.model.predict(sequence)

        temp_predictions = predictions.copy()

        for i in range(1, 11):
            index = np.argmax(temp_predictions)
            temp_predictions[0, index] = 0
            best_predictions.append(index)

        for predict in best_predictions:
            for key, value in self.tokenizer.word_index.items():
                if value == predict:
                    predicted_words.append(key)
                    break

        return predicted_words

    def try_to_predict(self, text):
        predicted_words = []
        try:
            words_list = text.split(" ")
            words_list = [re.sub("[\\s\\d\\\\\"\\[\\]+.*;:|!=@#$%^&()?/~{}]", '', word) for word in words_list]
            words_list = list(filter(None, words_list))

            if len(words_list) >= 3:
                cut_words_list = words_list[-3:]

                all_words_in_vocab = True

                for word in cut_words_list:
                    word_in_vocab = False

                    for key, value in self.tokenizer.word_index.items():
                        if key == word:
                            word_in_vocab = True
                            break

                    if not word_in_vocab:
                        all_words_in_vocab = False
                        break

                if all_words_in_vocab:
                    predicted_words = self.__predict_next_words(cut_words_list)

        except Exception as e:
            "Error occurred: " + str(e)
            return False, predicted_words

        return True, predicted_words

    def start_up(self):
        self.log.add_log(LogType.INFO, f"System startup...")
        self.__configure()
        model_loaded = self.__load_model_from_file()
        tokenizer_loaded = self.__load_tokenizer_from_file()

        if not model_loaded or not tokenizer_loaded:
            self.log.add_log(LogType.WARNING, "Model or tokenizer does not loaded. Try to teach network.")
            return False

        self.log.add_log(LogType.INFO, "Model and tokenizer loaded correctly.")

        self.log.add_log(LogType.INFO, f"Startup ended successfully... Network ready to use!")

        return True

    def add_data_from_file(self):
        self.log.add_log(LogType.INFO, "Trying to add new files into data directory...")
        self.log.add_log(LogType.INFO, "Selecting new data...")
        data_directory = 'Data'

        self.log.add_log(LogType.INFO, "Deleting old files...")
        files = glob.glob(f"{data_directory}/*")
        for f in files:
            os.remove(f)

        root = tk.Tk()
        root.withdraw()

        try:
            files = filedialog.askopenfilenames()
        except Exception as e:
            self.log.add_log(LogType.ERROR, f"Error occurred while trying to add new files to database: {str(e)}")
            return False

        if not files:
            self.log.add_log(LogType.INFO, "No files have been selected. Operation canceled.")
            return False

        for file in files:
            filename, file_extension = os.path.splitext(file)
            if file_extension != ".txt":
                self.log.add_log(LogType.WARNING, "Selected files are not .txt format! Operation canceled.")
                return False

        for file in files:
            shutil.copy(file, data_directory)

        self.log.add_log(LogType.INFO, "New files added successfully!")

        return True

    def prepare_data(self):
        self.log.add_log(LogType.INFO, "Preparing datasets from data directory...")
        data_loaded, data = self.__get_data_from_directory()

        if not data_loaded:
            self.log.add_log(LogType.WARNING, "Data directory empty! Operation canceled.")
            return False

        self.__delete_tokenizer_file()
        self.tokenizer = self.__create_tokenizer(data)
        x, y = self.__process_data(data)
        x_train, x_test, y_train, y_test = self.__split_data(x, y)

        x_train_path = 'x_train.csv'
        y_train_path = 'y_train.csv'
        x_test_path = 'x_test.csv'
        y_test_path = 'y_test.csv'

        train_dataset_saved = self.__save_data_to_csv(x_train, y_train, x_train_path, y_train_path)
        test_dataset_saved = self.__save_data_to_csv(x_test, y_test, x_test_path, y_test_path)
        if not train_dataset_saved or not test_dataset_saved:
            self.log.add_log(LogType.WARNING, "Problem occurred while saving datasets. Operation canceled.")
            self.__delete_datasets_csv_files()
            self.__delete_tokenizer_file()
            return False

        self.__delete_model_file()

        self.log.add_log(LogType.INFO, "Datasets prepared and processed. Network ready to learn!")
        return True

    def learn_network(self):
        self.log.add_log(LogType.INFO, "Trying to learn network...")

        x_train_path = 'x_train.csv'
        y_train_path = 'y_train.csv'
        x_test_path = 'x_test.csv'
        y_test_path = 'y_test.csv'

        train_dataset_loaded, x_train, y_train = self.__load_data_from_csv(x_train_path, y_train_path)
        test_dataset_loaded, x_test, y_test = self.__load_data_from_csv(x_test_path, y_test_path)

        if not train_dataset_loaded or not test_dataset_loaded:
            self.log.add_log(LogType.WARNING, "Problem occurred while loading datasets. Operation canceled.")
            return False

        tokenizer_loaded = self.__load_tokenizer_from_file()

        if not tokenizer_loaded:
            self.log.add_log(LogType.WARNING, "Tokenizer does not exist. Operation canceled.")
            return False

        vocabulary_size = len(self.tokenizer.word_index) + 1
        model_created, checkpoint = self.__create_model(vocabulary_size)

        if not model_created:
            self.log.add_log(LogType.WARNING, "Model does not exist. Operation canceled.")
            self.__delete_model_file()
            return False

        model_trained = self.__train_model(x_train, y_train, vocabulary_size, checkpoint)
        if not model_trained:
            self.log.add_log(LogType.WARNING, "Problem occurred while training model. Operation canceled.")
            self.__delete_model_file()
            return False

        model_tested = self.__test_model(x_test, y_test)
        if not model_tested:
            self.log.add_log(LogType.WARNING, "Problem occurred while testing model. Operation canceled.")
            self.__delete_model_file()
            return False

        self.log.add_log(LogType.INFO, "Network ready to use!")
        return True

    def test_network(self):
        self.log.add_log(LogType.INFO, "Trying to test network...")

        x_test_path = 'x_test.csv'
        y_test_path = 'y_test.csv'

        test_dataset_loaded, x_test, y_test = self.__load_data_from_csv(x_test_path, y_test_path)
        if not test_dataset_loaded:
            self.log.add_log(LogType.WARNING, "Problem occurred while loading dataset. Operation canceled.")
            return False

        if not self.model:
            self.log.add_log(LogType.WARNING, "Model does not exist. Operation canceled.")
            return False

        if not self.tokenizer:
            self.log.add_log(LogType.WARNING, "Tokenizer does not exist. Operation canceled.")
            return False

        model_tested = self.__test_model(x_test, y_test)
        if not model_tested:
            self.log.add_log(LogType.WARNING, "Problem occurred while testing model. Operation canceled.")
            return False

        self.log.add_log(LogType.INFO, "Network tested!")
        return True
