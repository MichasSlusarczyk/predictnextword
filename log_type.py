from enum import Enum


class LogType(Enum):
    INFO = 1
    WARNING = 2
    ERROR = 3
    EXCEPTION = 4
