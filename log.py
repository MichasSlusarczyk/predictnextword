from sqlite3 import Date


class Log:
    def __init__(self):
        self._log = ""
        self._observers = []

    @property
    def log(self):
        return self._log

    @log.setter
    def log(self, value):
        self._log = value
        for callback in self._observers:
            callback(self._log)

    def bind_to(self, callback):
        self._observers.append(callback)

    def add_log(self, log_type, log):
        self.log = f"< Date: {Date.today()} , Type: {log_type.name} > Message: {log}"
