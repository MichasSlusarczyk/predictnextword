import sys

import keyboard
from PyQt5.QtGui import QTextCursor
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5 import uic

from next_word_prediction import *
from update_insert_thread import UpdateInsertThread


class NextWordPredictionUI(QWidget):
    def __init__(self):
        super().__init__()

        uic.loadUi("PredictNextWord.ui", self)

        self.next_word_prediction = NextWordPrediction()
        self.next_word_prediction.log.bind_to(self.__log)
        self.app_ready = False

        self.__set_up_ui()

        self.selected_item = -1
        self.selected_item_temp = -1
        self.tab_deleting = False
        self.word_adding = False
        self.text_clearing = False

        self.thread = UpdateInsertThread()
        self.thread.update_insert.connect(self.__add_proposition_to_text)

        keyboard.on_press_key("down", self.__on_press_down)
        keyboard.on_press_key("up", self.__on_press_up)
        keyboard.on_press_key("tab", self.__on_press_tab)

        self.app_ready = self.next_word_prediction.start_up()

    def __set_up_ui(self):

        self.input_text_box.textChanged.connect(self.__input_text_box_text_changed)
        self.add_data_from_file_button.clicked.connect(self.__add_data_from_file_button_clicked)
        self.prepare_data_button.clicked.connect(self.__prepare_data_button_clicked)
        self.learn_network_button.clicked.connect(self.__learn_network_button_clicked)
        self.test_network_button.clicked.connect(self.__test_network_button_clicked)

    def __add_data_from_file_button_clicked(self):
        self.next_word_prediction.add_data_from_file()

    def __prepare_data_button_clicked(self):
        self.next_word_prediction.prepare_data()

    def __learn_network_button_clicked(self):
        self.app_ready = self.next_word_prediction.learn_network()

    def __test_network_button_clicked(self):
        self.next_word_prediction.test_network()

    def __input_text_box_text_changed(self):
        if not self.tab_deleting and not self.word_adding and not self.text_clearing:
            if self.propositions_list_widget \
                    and self.selected_item != -1 \
                    and self.input_text_box.toPlainText()[-1] == "\t":
                text = self.input_text_box.toPlainText()
                self.tab_deleting = True
                self.input_text_box.setText(text[:-1])
                self.tab_deleting = False

            if self.app_ready:
                predicted, predicted_words = self.next_word_prediction.try_to_predict(self.input_text_box.toPlainText())
                self.propositions_list_widget.clear()
                if predicted:
                    for word in predicted_words:
                        item = QListWidgetItem(word)
                        font = item.font()
                        font.setPointSize(12)
                        item.setFont(font)
                        item.setForeground(Qt.white)
                        item.setTextAlignment(Qt.AlignHCenter)
                        self.propositions_list_widget.addItem(item)
                    self.__update_item_list()
            else:
                if self.input_text_box.toPlainText() != "":
                    self.text_clearing = True
                    self.input_text_box.setText("")
                    self.text_clearing = False

                self.__add_log(LogType.WARNING, "Model is not ready to use!")

    def __log(self, log):
        if not self.log_label.toPlainText():
            self.log_label.setText(log)
        else:
            self.log_label.setText(self.log_label.toPlainText() + '\n' + log)

    def __add_log(self, log_type, log):
        self.next_word_prediction.log.add_log(log_type, log)

    def __on_press_down(self, key):
        if self.selected_item == 9:
            self.selected_item = -1
        else:
            self.selected_item += 1

        self.__update_item_list()

    def __on_press_up(self, key):
        if self.selected_item == -1:
            self.selected_item = 9
        else:
            self.selected_item -= 1

        self.__update_item_list()

    def __on_press_tab(self, key):
        self.thread.start()
        return

    def __update_item_list(self):
        if self.propositions_list_widget:
            for it in range(self.propositions_list_widget.count()):
                item = self.propositions_list_widget.takeItem(it)
                item.setForeground(Qt.white)
                self.propositions_list_widget.insertItem(it, item)

            if self.selected_item != -1:
                item = self.propositions_list_widget.takeItem(self.selected_item)
                item.setForeground(Qt.lightGray)
                self.propositions_list_widget.insertItem(self.selected_item, item)

    def __add_proposition_to_text(self):
        if self.propositions_list_widget and self.selected_item != -1:
            item = self.propositions_list_widget.takeItem(self.selected_item)
            self.word_adding = True
            self.input_text_box.setPlainText(f"{self.input_text_box.toPlainText()}{item.text()}")
            self.word_adding = False
            self.propositions_list_widget.insertItem(self.selected_item, item)
            cursor = QTextCursor(self.input_text_box.textCursor())
            cursor.movePosition(QTextCursor.End)
            self.input_text_box.setTextCursor(cursor)
            self.selected_item = -1
            self.__update_item_list()
            self.__input_text_box_text_changed()


app = QApplication(sys.argv)
window = NextWordPredictionUI()
window.show()
sys.exit(app.exec())
